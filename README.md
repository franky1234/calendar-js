# Instructions

## Custom calendar

- Open index.html in a web browser.

- Click on the arrow previous & next to change month.

- Click on a date to see the changed text date.
