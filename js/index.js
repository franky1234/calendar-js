const week = [
  { day: 'Domingo', abbreviation: 'Dom', value: 0 },
  { day: 'Lunes', abbreviation: 'Lu', value: 1 },
  { day: 'Martes', abbreviation: 'Ma', value: 2 },
  { day: 'Miercoles', abbreviation: 'Mie', value: 3 },
  { day: 'Jueves', abbreviation: 'Jue', value: 4 },
  { day: 'Viernes', abbreviation: 'Vie', value: 5 },
  { day: 'Sabado', abbreviation: 'Sab', value: 6 },
];

const months = [
  { month: 'Enero', abbreviation: 'Ene', value: 0 },
  { month: 'Febrero', abbreviation: 'Feb', value: 1 },
  { month: 'Marzo', abbreviation: 'Mar', value: 2 },
  { month: 'Abril', abbreviation: 'Abr', value: 3 },
  { month: 'Mayo', abbreviation: 'May', value: 4 },
  { month: 'Junio', abbreviation: 'Jun', value: 5 },
  { month: 'Julio', abbreviation: 'Jul', value: 6 },
  { month: 'Agosto', abbreviation: 'Ago', value: 7 },
  { month: 'Septiembre', abbreviation: 'Sept', value: 8 },
  { month: 'Octubre', abbreviation: 'Oct', value: 9 },
  { month: 'Noviembre', abbreviation: 'Nov', value: 10 },
  { month: 'Deciembre', abbreviation: 'Dic', value: 11 },
];

const MONTH_ROWS = week.length - 1;
const MONTH_COLS = week.length;
const INFERIOR_LIMIT_MONTHS = 1;
const SUPERIO_LIMIT_MONTHS = months.length;

let calendarDate = {
  year: null,
  month: null,
  current: null,
  startDate: null,
  endDate: null,
};

const createHeaderWeek = () => {
  const tableDateHeader = document.querySelector('.table-date-header');
  const dayRow = document.createElement('tr');
  week.forEach(({ day }) => {
    const dayCell = document.createElement('th');
    const textCell = document.createTextNode(day);
    dayCell.appendChild(textCell);
    dayRow.appendChild(dayCell);
  });
  tableDateHeader.appendChild(dayRow);
};

const getMonthDateRange = ({ year, month }) => {
  const startDate = moment([year, month]);
  const endDate = moment(startDate).endOf('month');
  return {
    startDate: {
      dayOfWeek: startDate.day(),
      date: startDate.date(),
    },
    endDate: {
      dayOfWeek: endDate.day(),
      date: endDate.date(),
    },
  };
};

const generateDateMonth = (currentDate) => {
  const date = {
    momentMonth: currentDate.month() + 1,
    month: currentDate.month(),
    year: currentDate.year(),
    current: currentDate.date(),
  };
  const rangeMonth = getMonthDateRange(date);
  return { ...date, ...rangeMonth };
};

const generateTableDate = ({ month, year, current, startDate, endDate }) => {
  const { dayOfWeek: startDayOfWeek, date: initDate } = startDate;
  const { date: finalDate } = endDate;

  let counterDay = initDate;
  let daySelected = current;

  const tableBody = document.querySelector('.table-date-body');

  for (let i = 0; i < MONTH_ROWS; i++) {
    const tableBodyRow = document.createElement('tr');
    tableBodyRow.className = `table-row-${i}`;
    tableBodyRow.id = `row-${i}`;
    for (let j = 0; j < MONTH_COLS; j++) {
      const tableBodyColumn = document.createElement('td');
      tableBodyColumn.className = `table-column-${j}`;
      tableBodyColumn.id = `row${i} col-${j}`;
      let textColumn = document.createTextNode('');
      if (i === 0 && j >= startDayOfWeek) {
        textColumn = document.createTextNode(counterDay);
        counterDay += 1;
      } else if (i > 0 && counterDay <= finalDate) {
        textColumn = document.createTextNode(counterDay);
        counterDay += 1;
      } else if (i === 0) {
        // textColumn = document.createTextNode('unused start days');
      } else {
        // textColumn = document.createTextNode('unused end days');
      }
      if (counterDay === current + 1) {
        daySelected = j;
        currentSelector = tableBodyColumn;
        tableBodyColumn.className = `${tableBodyColumn.className} selected`;
      }

      tableBodyColumn.onclick = () => {
        const currentSelected = document.getElementById(`row${i} col-${j}`);
        if (currentSelected.textContent) {
          const previousColSelected = document.querySelector('.selected');
          previousColSelected.className = `table-column-${j}`;
          daySelected = j;
          currentSelected.className = `table-column-${j} selected`;
          current = currentSelected.textContent;
          formatDateSelected({ month, year, current, daySelected });
        }
      };

      tableBodyColumn.appendChild(textColumn);
      tableBodyRow.appendChild(tableBodyColumn);
    }
    tableBody.appendChild(tableBodyRow);
  }
  formatDateSelected({ month, year, current, daySelected });
};

const formatDateSelected = ({ month, year, current, daySelected }) => {
  const { day } = week[daySelected];
  const { month: selectedMonth, abbreviation } = months[month];
  const formattedDate = `${day}, ${current} de ${selectedMonth} del ${year}`;
  const formattedTitleDate = `${abbreviation} ${year}`;
  document.querySelector('.date-text').innerHTML = formattedDate;
  document.querySelector('.title-month').innerHTML = formattedTitleDate;
};

const removeTableBody = () => {
  const tableBody = document.querySelector('.table-date-body');
  while (tableBody.hasChildNodes()) {
    tableBody.removeChild(tableBody.lastChild);
  }
};

window.onload = () => {
  createHeaderWeek();
  calendarDate = generateDateMonth(moment());
  generateTableDate(calendarDate);
  calendarDate.month += 1;
  const previous = document.querySelector('.previous');

  previous.onclick = () => {
    calendarDate.momentMonth -= 1;
    if (calendarDate.momentMonth < 1) {
      calendarDate.momentMonth = SUPERIO_LIMIT_MONTHS;
      calendarDate.year -= 1;
    }
    calendarDate = generateDateMonth(
      moment(`${calendarDate.year}-${calendarDate.momentMonth}-${1}`, [
        'YYYY-MM-DD',
      ])
    );

    removeTableBody();
    generateTableDate(calendarDate);
  };

  const next = document.querySelector('.next');
  next.onclick = () => {
    calendarDate.momentMonth += 1;
    if (calendarDate.momentMonth >= SUPERIO_LIMIT_MONTHS) {
      calendarDate.momentMonth = INFERIOR_LIMIT_MONTHS;
      calendarDate.year += 1;
    }
    calendarDate = generateDateMonth(
      moment(`${calendarDate.year}-${calendarDate.momentMonth}-${1}`, [
        'YYYY-MM-DD',
      ])
    );
    removeTableBody();
    generateTableDate(calendarDate);
  };
};
